![coverphoto](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/juvarabrera-wallpaper.png)

# Welcome to my GitLab Profile!

<p style="text-align: justify;">
Full-Stack Lead Web Developer with experience working on projects for the National Telehealth Center (NTHC) and the Information Management Service (IMS) at the University of the Philippines Manila. Also serving as Chief Technology Officer for two startups, Pivotal Peak Digital Health Solutions Inc. and Yakap Health Inc.

Highly skilled and experienced full-stack web developer with a strong track record of successfully leading development teams to produce high-quality web applications. Proficient in facilitating design thinking workshops, documenting business and technical requirements, prototyping, managing tasks with GitLab, developing solutions with Python Django, MySQL, React, and Docker, testing applications with Rainforest QA, deploying solutions to cloud-based or Linux servers, and tracking uptime with Uptime Kuma. Exceptional ability to communicate with clients, present updates, and generate reports.
</p>
<p style="text-align: center;">
![work](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/msedge_IUNLwH6rZo.png)
</p>

## Main Responsibilities

- **Facilitate** design thinking workshop or system specifications gathering meetings with clients
- **Document** business requirements document, system design, technical requirements document and functional requirements document
- **Design** low-fidelity prototype using Diagrams.net, and mid-fidelity prototype using Adobe Xd and get feedback from clients and revise the prototype
- **Convert** mid-fidelity prototype to high fidelity prototype using HTML, CSS, Bootstrap, Javascript
- **Lead** small team of developers to produce a high-quality web application fast and secure
- **Manage, organize, and track** issues/tasks using GitLab and assign to team members
- **Develop** the solution using different technologies, primarily, Python Django, MySQL, React, Docker
- **Test** the application automatically using Rainforest QA
- **Deploy** the solution into a cloud-based environment like AWS EC2 or our own physical Linux-based server via ssh or lxc
- **Track** uptime of products and applications using Uptime Kuma
- **Present** updates and generate reports for the client

## Skills

<p style="text-align: center;">
![skills](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/msedge_DweurEfUt8.png)
</p>

## Contributions History

<p style="text-align: center;">
![contribution2015](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2015.png)

![contribution2016](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2016.png)

![contribution2017](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2017.png)

![contribution2018](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2018.png)

![contribution2019](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2019.png)

![contribution2020](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2020.png)

![contribution2021](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2021.png)

![contribution2022](https://gitlab.com/juvarabrera/juvarabrera/-/raw/main/contribution2022.png)
</p>

## Links

- [LinkedIn](https://www.juvarabrera.com/linkedin)
- [Twitter](https://www.twitter.com/juvarabrera)
- [PGP](https://keyserver.ubuntu.com/pks/lookup?search=6E04572601E02011&fingerprint=on&op=index)
- [HackerRank](https://www.hackerrank.com/juvarabrera)
- [Codecademy](https://www.codecademy.com/profiles/juvarabrera)
